#!/usr/bin/env python3

from typing import *

import gi,pathlib,copy,json

gi.require_version("Gtk","3.0")
from gi.repository import Gtk,Gdk

class Settings:
	def __init__(self,settingsfile):
		self.__data={
			"filename":str(pathlib.Path.home())+"/.tasks.txt",
			"newitems":"prepend",
			"ontop":True,
			"focus":False,
			"notewidth":200,
			"noteheight":300,
		}
		try:
			with open(settingsfile,"r") as file:
				self.__data=json.loads(file.read())
		except FileNotFoundError:
			with open(settingsfile,"w") as file:
				file.write(json.dumps(self.__data))

		self.__data["settingsfile"]=settingsfile

	@property
	def settingsfile(self):
		return self.__data["settingsfile"]

	@property
	def filename(self):
		return self.__data["filename"]

	@filename.setter
	def filename(self,value):
		self.__data["filename"]=value

	@property
	def newitems(self):
		return self.__data["newitems"]

	@newitems.setter
	def newitems(self,value):
		self.__data["newitems"]=value

	@property
	def ontop(self):
		return self.__data["ontop"]

	@ontop.setter
	def ontop(self,value):
		self.__data["ontop"]=value

	@property
	def focus(self):
		return self.__data["focus"]

	@focus.setter
	def focus(self,value):
		self.__data["focus"]=value

	@property
	def notewidth(self):
		return self.__data["notewidth"]

	@notewidth.setter
	def notewidth(self,value):
		self.__data["notewidth"]=value

	@property
	def noteheight(self):
		return self.__data["noteheight"]

	@noteheight.setter
	def noteheight(self,value):
		self.__data["noteheight"]=value

class Datastore:
	"""
	Handles maintaining the internal data of the app and keeping it in sync between GTK and file representations.

	Aggressively file-synced to help avoid data loss.
	"""
	def __init__(self,settingsfile):
		self.__settings=Settings(settingsfile)
		self.__data: List[Union[Tuple[str,str],None]]=[]
		self.__load()

	def __getitem__(self,index: int):
		self.__load()
		return self.__data[index]

	def __setitem__(self,index: int,value: Union[Tuple[str,str],None]):
		self.__data[index]=value
		self.__save()

	def __deformat(self,lines: List[str]) -> List[Union[Tuple[str,str],None]]:
		title=None
		desc=None
		data: List[Union[Tuple[str,str],None]]=[]
		for line in lines:
			if len(line)>0:
				if line[0]=="\t":
					desc+=line[1:]+"\n"
				else:
					if title is not None:
						data.append((title,desc))
					title=line
					desc=""
		if title is not None:
			data.append((title.strip(),desc.strip()))
		return data

	def __format(self,data: List[Union[Tuple[str,str],None]]) -> List[str]:
		lines: List[str]=[]
		for record in data:
			if record is not None:
				lines.append("".join(record[0].strip().split("\t")))
				for line in record[1].strip().split("\n"):
					lines.append("\t"+line)
		return lines

	def __load(self):
		try:
			with open(self.__settings.filename,"r") as file:
				self.__data=self.__deformat(file.read().split("\n"))
		except FileNotFoundError:
			self.__data=[]

	def __save(self):
		with open(self.__settings.filename,"w+") as file:
			file.write("\n".join(self.__format(self.__data)))

	@property
	def settings(self):
		return self.__settings

	def append(self,value: Union[Tuple[str,str],None]) -> None:
		if self.__settings.newitems=="prepend":
			self.__data.insert(0,value)
		else:
			self.__data.append(value)
		self.__save()

	def backsync(self,liststore: Gtk.ListStore) -> None:
		"""
		The most complicated thing this class does is taking in a GTK data store and figuring out what changed after the user reorders elements or similar
		"""
		# copy data now that corresponds to current state of data matching liststore
		olddata=copy.deepcopy(self.__data)
		# detail windows indices will no longer be valid after this operation, close them now to ensure the changes are synced to data
		icon.close_details()
		newdata=[]
		iter=liststore.get_iter_first()
		while iter is not None:
			newdata.append((liststore.get(iter,0)[0],liststore.get(iter,1)[0]))
			iter=liststore.iter_next(iter)

		# go through the original data that corresponds to the liststore to find what moved where
		mapping={}
		xcount=0
		for x in olddata:
			ycount=0
			for y in newdata:
				if x[0].strip()==y[0].strip() and x[1].strip()==y[1].strip() and ycount not in mapping.keys():
					mapping[ycount]=xcount
				ycount+=1
			xcount+=1

		# now load the changed data from the current field in the order specified by the liststore
		newindices=[x for x in mapping.keys()]
		newindices.sort()
		data=[]
		for newindex in newindices:
			data.append(self.__data[mapping[newindex]])

		self.__data=data
		self.__save()

	def create_liststore(self) -> Gtk.ListStore:
		"""
		Return the current data in a format GTK will understand
		"""
		self.__load()
		liststore=Gtk.ListStore(str,str)
		for x in self.__data:
			if x is not None:
				liststore.append(x)
		return liststore

	def reload_config(self):
		self.__settings=Settings(self.__settings.settingsfile)

class Icon:
	"""
	Displays the app in the system tray and handles showing/hiding windows (notes/dropdown)
	"""
	def __init__(self) -> None:
		self.__win=None
		self.__details=[]
		self.__icon=Gtk.StatusIcon()
		self.__icon.set_from_icon_name("gtk-edit")

		def handle_menu(*args,**kwargs):
			return self.__handle_menu(*args,**kwargs)

		self.__icon.connect("popup-menu",handle_menu)

		def handle_activate(*args,**kwargs):
			return self.__handle_activate(*args,**kwargs)

		self.__icon.connect("activate",handle_activate)

	def __handle_activate(self,*args,**kwargs) -> None:
		if self.__win is not None and not self.__win.destroyed:
			self.destroy_window()
		else:
			self.__win=Window()
			self.__win.show()
			self.__win.move(self.__icon.get_geometry()[2].x,self.__icon.get_geometry()[2].y)
			self.__win.focus()

	def __handle_menu(self,data,button,event_time,*args,**kwargs) -> None:
		menu=Gtk.Menu()
		settings_item=Gtk.MenuItem("Reload Config")
		close_item=Gtk.MenuItem("Quit")

		menu.append(settings_item)
		menu.append(close_item)

		def handle_quit(*args,**kwargs):
			return self.__handle_quit(*args,**kwargs)
		def handle_settings(*args,**kwargs):
			return self.__handle_settings(*args,**kwargs)

		close_item.connect_object("activate",handle_quit,"Close App")
		close_item.show()

		settings_item.connect_object("activate",handle_settings,"Reload config file "+datastore.settings.settingsfile)
		settings_item.show()

		menu.popup(None,None,self.__icon.position_menu,data,button,event_time)

	def __handle_quit(self,*args,**kwargs) -> None:
		quit(0)

	def __handle_settings(self,*args,**kwargs) -> None:
		datastore.reload_config()

	def close_details(self) -> None:
		for win in self.__details:
			if not win.destroyed:
				win.destroy()
		self.__details=[]

	def destroy_window(self) -> None:
		self.__win.destroy()

	def register_detail_window(self,win) -> None:
		self.__details.append(win)

class Window:
	"""
	Main dropdown with task list
	"""
	def __init__(self) -> None:
		self.destroyed=False
		self.__win=Gtk.Window()
		self.__treeview=Gtk.TreeView()
		self.__input=Gtk.Entry()

		self.__input.set_tooltip_text("New Task Title")
		self.__treeview.set_property("reorderable",True)

		self.__win.set_decorated(False)
		self.__win.set_deletable(False)
		self.__win.set_keep_above(True)
		self.__win.set_type_hint(Gdk.WindowTypeHint.DROPDOWN_MENU)
		self.__win.set_skip_taskbar_hint(True)
		self.__win.set_skip_pager_hint(True)

		self.__treeview.set_model(datastore.create_liststore())
		self.__treeview.set_headers_visible(False)
		self.__treeview.append_column(Gtk.TreeViewColumn("",Gtk.CellRendererText(),text=0))

		def handle_row_activated(*args,**kwargs):
			return self.__handle_row_activated(*args,**kwargs)

		self.__treeview.connect("row-activated",handle_row_activated)

		def handle_enter(*args,**kwargs):
			return self.__handle_enter(*args,**kwargs)

		self.__input.connect("activate",handle_enter)

		def handle_focusout(*args,**kwargs):
			return self.__handle_focusout(*args,**kwargs)

		self.__win.connect("focus-out-event",handle_focusout)

		def handle_delete(*args,**kwargs):
			return self.__handle_delete(*args,**kwargs)

		self.__win.connect("delete-event",handle_delete)
		self.__win.connect("destroy",handle_delete)
		self.__win.connect("destroy-event",handle_delete)

		def handle_reorder(*args,**kwargs) -> None:
			self.__handle_reorder(*args,**kwargs)

		self.__treeview.connect("drag-end",handle_reorder)

		self.__box=Gtk.VBox()
		self.__box.pack_start(self.__input,False,False,0)
		self.__box.pack_start(self.__treeview,False,False,0)
		self.__win.add(self.__box)

		self.__win.show_all()

	def __get_new_text(self) -> str:
		data=self.__input.get_text()
		self.__input.set_text("")
		return data

	def __handle_delete(self,*args,**kwargs) -> None:
		self.destroyed=True

	def __handle_detail_sync(self,*args,**kwargs) -> None:
		# if we in the future want to keep the window open here, we will need to ensure the data is properly synced back before creating a new liststore
		icon.destroy_window()

	def __handle_enter(self,*args,**kwargs) -> None:
		datastore.append((self.__get_new_text(),""))
		self.__treeview.set_model(datastore.create_liststore())

	def __handle_focusout(self,*args,**kwargs) -> None:
		self.__win.destroy()
		self.destroyed=True

	def __handle_reorder(self,*args,**kwargs) -> None:
		datastore.backsync(self.__treeview.get_model())

	def __handle_row_activated(self,treeview,path,column) -> None:
		d=Detail(path.get_indices()[-1])

		def handle_detail_sync(*args,**kwargs):
			return self.__handle_detail_sync(*args,**kwargs)

		d.connect("delete-event",handle_detail_sync)
		d.connect("destroy",handle_detail_sync)
		d.connect("destroy-event",handle_detail_sync)
		d.show()
		d.focus()

	def destroy(self) -> None:
		self.__win.destroy()

	def focus(self) -> None:
		self.__win.present()

	def get_visible(self) -> None:
		return self.__win.get_visible()

	def move(self,*args,**kwargs) -> None:
		self.__win.move(*args,**kwargs)

	def show(self) -> None:
		self.__win.show()

class Detail:
	"""
	Sticky note window
	"""
	def __init__(self,index) -> None:
		icon.register_detail_window(self)
		self.destroyed=False
		self.__synced=False
		self.__title=Gtk.Entry()
		self.__title.set_text(datastore[index][0])
		self.__text=Gtk.TextView()
		self.__text.get_buffer().set_text(datastore[index][1].strip())
		self.__box=Gtk.VBox()
		self.__win=Gtk.Window()
		self.__index=index

		self.__title.set_tooltip_text("Task Title (erase to delete)")
		self.__text.set_tooltip_text("Task Description")

		self.__box.pack_start(self.__title,False,False,0)
		self.__box.pack_start(self.__text,True,True,0)
		self.__win.add(self.__box)

		if datastore.settings.ontop:
			self.__win.set_keep_above(True)
		self.__win.set_property("default-width",datastore.settings.notewidth)
		self.__win.set_property("default-height",datastore.settings.noteheight)

		def handle_delete(*args,**kwargs):
			return self.__handle_delete(*args,**kwargs)

		self.__win.connect("delete-event",handle_delete)
		self.__win.connect("destroy",handle_delete)
		self.__win.connect("destroy-event",handle_delete)

		def destroy(*args,**kwargs):
			return self.destroy(*args,**kwargs)

		self.__title.connect("activate",destroy)


		def handle_focusout(*args,**kwargs):
			return self.__handle_focusout(*args,**kwargs)

		if datastore.settings.focus:
			self.__win.connect("focus-out-event",handle_focusout)

		self.__win.show_all()

	def __handle_delete(self,*args,**kwargs) -> None:
		self.destroyed=True
		if not self.__synced:
			self.__synced=True
			title=self.__title.get_text()
			if title!="":
				datastore[self.__index]=(title,self.__text.get_buffer().get_text(self.__text.get_buffer().get_bounds()[0],self.__text.get_buffer().get_bounds()[1],False))
			else:
				datastore[self.__index]=None

	def __handle_focusout(self,*args,**kwargs) -> None:
		self.__win.destroy()
		self.destroyed=True

	def connect(self,*args,**kwargs) -> None:
		self.__win.connect(*args,**kwargs)

	def destroy(self,*args,**kwargs) -> None:
		self.__win.destroy()
		self.destroyed=True

	def focus(self) -> None:
		self.__win.present()

	def hide(self) -> None:
		self.__win.hide()

	def show(self) -> None:
		self.__win.show()

if __name__=="__main__":
	datastore=Datastore(str(pathlib.Path.home())+"/.pytask.json")
	icon=Icon()
	Gtk.main()

Yet another "notes" app with the specific goal of replacing a todo.txt and providing only slightly more intelligent structure.

Main added features over a text document:
- system tray icon
- a distinction between task name and description (description is optional)
- easily reorderable entries
- constantly synced with filesystem in case of power loss or crashes

Maintained/emphasized features of a text document:
- easy to add new entries and modify existing ones -- just type, no buttons, only one "drill-down" that's totally optional
- freeform, no special fields or relationships like "project" that have to be created a certain way
- human-readable serialized format (~/.tasks.txt)

To open the tasks list, click the system tray icon.

To create a new task, type a title into the text box and press enter.

To open a task's "sticky note", double-click on its entry in the task list.

To rename a task, change the title text in the smaller text field at the top of the sticky note.

To add a description to a task, type into the larger text field in the sticky note.

To delete a task, remove all text from its title field in the sticky note.

To save your sticky note changes, close the sticky note.

To reorder task list entries, click and drag.

To close pytask, right click the system tray icon and choose Quit.
